# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			178/01/2019		
#DESCRIPCIO: 14-popen-sql-multi.py  -d database   [-c numclie]..
###############################################################################################################################################
#Imports
import sys, argparse
from subprocess import Popen, PIPE
###############################################################################################################################################
#COntrol d'arguments
parser = argparse.ArgumentParser(description="""Sentencia sql""")
parser.add_argument("-d",type=str,help="Tens que posar una taula amb -d",metavar="bbdd")
parser.add_argument("-c",type=int,help="Pnum_clie Posar numero de client",metavar="client",required="True",action="append")
args=parser.parse_args()
###############################################################################################################################################
#Xixa
command="psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 " + (args.bbdd)
pipeData = Popen(command, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
#pipeData.stdin.write(command+select)
for line in args.client:
	select= "-c 'Select * from clientes WHERE num_clie = %d ;'" % (line)
	pipeData.stdin.write(select+"\n")
	print pipeData.stdout.readline()
pipeData.stdin.write("\q\n")
exit(0)
###############################################################################################################################################
