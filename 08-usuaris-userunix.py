# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			11/01/2019		
#DESCRIPCIO: Exemlples d'arguments
###############################################################################################################################################
#Imports
import argparse
import sys
###############################################################################################################################################
#Control d'arguments
parser = argparse.ArgumentParser(description= """Mostrar les N primereslínies """, epilog="thats all folks")
parser.add_argument("-f","--fit",type=str, help="fitxer a processar", metavar="file", default="/dev/stdin",dest="fitxer")
args = parser.parse_args()
###############################################################################################################################################
#Implementació de objecte
def compara_gid(gid_a,gid_b):
  if gid_a > gid_b:
    return 1
  if gid_a < gid_b:
    return -1
  if gid_a > gid_b:
    return 1
  if gid_a < gid_b:
    return -1
  return 0
###############################################################################################################################################
def compara_login(login_a,login_b):
  
  if login_a >login_b:
    return 1
  if login_a < login_b:
    return -1
  return 0
###############################################################################################################################################
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,linea):
    "Constructor objectes UnixUser"
    llista_user = linea.split(":")
    self.login= llista_user[0]
    self.passw= llista_user[1]
    self.uid= llista_user[2]
    self.gid= llista_user[3]
    self.gecos = llista_user[4]
    self.home = llista_user[5]
    self.shell = llista_user[6]
  def show(self):
    "Mostra dades del usuari"
    print "login: %s, %s, %d, %d, %s, %s, %s" % (self.login, self.passw, int(self.uid), int(self.gid), self.gecos, self.home, self.shell)
  def sumaun(self):
    "Funcio que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    return "%s, %s, %d, %d, %s, %s, %s" % (self.login, self.passw, int(self.uid), int(self.gid), self.gecos, self.home, self.shell)
  def comp_gid(self,llista_user,camp):
    "FUncio que compara gid"
###############################################################################################################################################
#Programa
filein = open (args.fitxer,"r")
userlist = [] 
for line in filein:
	line_user = UnixUser(line)
	userlist.append(line_user)
filein.close()
print userlist
for user in userlist:
	print user
exit(0)
###############################################################################################################################################
