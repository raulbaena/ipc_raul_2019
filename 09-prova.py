# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			11/01/2019		
#DESCRIPCIO: Exemlples d'arguments
##############################################################################################################################################################################################################################################################################################
class UnixGroup():
	'''Clse unix group Dtipus mysql:x:27:gid:usuaris'''
	"Constructor objectes UnixGroup"
	def __init__(self,linea):
		nothing = '\n'
		llista_group = linea.split(":")
		self.gname = llista_group[0]
		self.passwd = llista_group[1]
		self.gid = llista_group[2]
		self.usuaris = llista_group[3]
		self.users = self.usuaris.split(',')
	def show(self):
		print "Grup: %s %s %d %s" % (self.gname, self.passwd, int(self.gid), self.users)
	def __str__(self):
		return " %s %s %d %s" % (self.gname, self.passwd, int(self.gid), self.users)


###############################################################################################################################################
filein = open("/etc/group","r")

for group in filein:
	group_now = UnixGroup(group) 
	print group_now
filein.close()