# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			16/01/2019		
#DESCRIPCIO: 10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups.
#LListar els grups del sistema ordenats pel criteri de gname, gid o de número d'usuaris.
#Atenció cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris
#que hi pertanyin, sense duplicitats, tant com a grup principal com a
#grup secundari.
###############################################################################################################################################
#Imports
import argparse
import sys
groupDict={}
###############################################################################################################################################
#Control d'arguments
parser = argparse.ArgumentParser(description="""Llistar els usuaris de file o stdin (format /etc/passwd""",epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,help="sort criteri: login | gid | gname", metavar="criteri",choices=["login","gid","gname"],dest="criteri")
parser.add_argument("-u","--userfile",type=str,help="user file (/etc/passwd style)", metavar="userFile")
parser.add_argument("-g","--groupfile",type=str,help="user file (/etc/passwd style)", metavar="groupFile")
args=parser.parse_args()
###############################################################################################################################################
#Classes
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDict:
      self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print "login: %s, uid:%d, gid=%d" % \
           (self.login, self.passwd, self.uid, self.gid, self.gname,\
            self.gecos, self.home, self.shell)
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
    "functió to_string d'un objcete UnixUser"
    return "%s %d %d %s" % (self.login, self.uid, self.gid, self.gname)

class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
  gname_passwd:gid:listUsers"""
  def __init__(self,groupLine):
    "Constructor objectes UnixGroup"
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.userListStr = groupField[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "functió to_string d'un objecte UnixGroup"
    return "%s %d %s" % (self.gname, int(self.gid), self.userList)
###############################################################################################################################################
#Xixa 
#Per cada linea d'ususaris crea un diccionari tipus amb la clau gid y el valor una class tipus UnixGroup
groupFile=open(args.groupFile,"r")
for line in groupFile:
  group=UnixGroup(line)
  groupDict[group.gid]=group
groupFile.close()
#Per cada us
userFile=open(args.userFile,"r")
userList=[]
for line in userFile:
  user=UnixUser(line)
  userList.append(user)
  if user.gid in groupDict:
  	if user.login not in groupDict[user.gid].userList:
      groupDict[user.gid].userList.append(user.login)
userFile.close()
index=[]
if args.criteri=="gid":
  index = groupDict.keys()
elif args.criteri=="gname":
  index = [ (groupDict[k].gname,k) for k in groupDict ]
elif args.criteri=="nusers":
  index = [ (len(groupDict[k].userList),k) for k in groupDict ]
index.sort()
if args.criteri=="gid":
  for k in index:
   print groupDict[k]
else:
  for c,k in index:
   print groupDict[k]
exit(0)
