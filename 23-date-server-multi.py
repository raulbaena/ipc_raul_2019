# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			25/01/2019		
#DESCRIPCIO: 23-date-server-multi
###############################################################################################################################################
#Imports
import sys,socket
from subprocess import Popen, PIPE
###############################################################################################################################################
import sys,socket
HOST = ''
PORT = 50001
#Creem el socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#Encadenem 
s.bind((HOST, PORT))
#Escoltem
s.listen(1)
while True:
	#Acceptem la conexio amb el client
	conn, addr = s.accept()
	print "CONNCTED BY", addr
	#Executem la comanda del date
	command = ['date']
	pipeData = Popen(command,stdout=PIPE)
	#Enviem la sortida de la comanda al client
	for line in pipeData.stdout:
		conn.send(line)
	conn.close()
sys.exit(0)
