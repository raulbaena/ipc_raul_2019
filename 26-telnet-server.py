# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			178/01/2019		
#DESCRIPCIO: 26-telnet-server
###############################################################################################################################################
#Imports
import sys,socket,argparse,os,signal
from subprocess import Popen, PIPE
###############################################################################################################################################
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""Telnet""")
parser.add_argument("-p","--port",type=int, default=50001)
#parser.add_argument("-d","--debub",type=str, default=50001, dest='debug',default='2019')
args=parser.parse_args()
llistaPeers=[]
HOST = ''
PORT = args.port

def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  print len(llistaPeers)
  sys.exit(0)

def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers, len(llistaPeers)
  sys.exit(0)
  
#pid=os.fork()
#if pid !=0:
#  print "Engegat el server telnet:", pid
#  sys.exit(0)
  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print "Connected by", addr
    llistaPeers.append(addr)
    while True:
        data = conn.recv(1024)
        command = data
        if not data:
            break
        pipeData = Popen(command,shell=True,stdout=PIPE)
        for line in pipeData.stdout:
            conn.send(line)
        conn.send(chr(4))
	conn.close()
sys.exit(0)

