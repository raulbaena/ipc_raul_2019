# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			23/01/2019		
#DESCRIPCIO: Exemople fork
###############################################################################################################################################
#Imports
import sys, os, signal, argparse
###############################################################################################################################################
#Funcions
def myusr1(signum,frame):
	print "Hem regut alarma", signum
	print "Te voy a hacer un fork morena"
	
def myusr2(signum,frame):
	print "Hem regut alarma", signum
	print "Hasta luego maricarmen Tururu"
	sys.exit(0)
###############################################################################################################################################
#Printem el pid pare
print "Hola comensament del programa principal"
print "PID pare:", os.getpid()

#Ceem el proces fill
pid = os.fork()
if pid != 0:
	print "Programa Pare", os.getpid(), pid
	sys.exit(0)
	
signal.signal(signal.SIGUSR2,myusr2)
signal.signal(signal.SIGUSR1,myusr1)

#Printem el pid fill
print "Programa Fill", os.getpid(), pid
while True:
	pass
		
