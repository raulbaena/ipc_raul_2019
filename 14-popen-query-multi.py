# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			178/01/2019		
#DESCRIPCIO: 14-popen-sql-multi.py  -d database   [-c numclie]..
###############################################################################################################################################
#Imports
import sys, argparse
from subprocess import Popen, PIPE
###############################################################################################################################################
#COntrol d'arguments
parser = argparse.ArgumentParser(description="""Sentencia sql""")
parser.add_argument("-d",type=str,help="Tens que posar una taula amb -d",dest="base")
parser.add_argument("-c",type=int,help="Pnum_clie Posar numero de client",dest="client",action="append")
args=parser.parse_args()
###############################################################################################################################################
#Xixa
command="psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 " + (args.base)
pipeData = Popen(command,stdout=PIPE,stdin=PIPE,stderr=PIPE,shell=True)
for line in args.client:
	select= "Select * from clientes WHERE num_clie =%d" % (line)
	pipeData.stdin.write(select)
	print pipeData.stdout.readline()
exit(0)