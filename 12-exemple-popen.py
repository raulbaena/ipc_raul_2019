# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-popen.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
# -------------------------------------------------------
command = ["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"select * from clientes;\"","]
#Hem creat un pipe que executa el contingut de la variable command
pipeData = Popen(command,stdout=PIPE)
for line in pipeData.stdout:
	print line
exit(0)

