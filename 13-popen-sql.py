# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			178/01/2019		
#DESCRIPCIO: -13-popen-sql-inject.py Senteciasql
###############################################################################################################################################
#Imports
import sys, argparse
from subprocess import Popen, PIPE
###############################################################################################################################################
#COntrol d'arguments
parser = argparse.ArgumentParser(description="""Senencia sql""")
parser.add_argument("select",type=str,help="Sentencia sql")
args=parser.parse_args()
###############################################################################################################################################
#Programa
#command = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c '%s'" % (args.select)
#pipeData = Popen(command,stdout=PIPE,shell=True)

#Forma Eduard
command = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training"
pipeData = Popen(command,stdout=PIPE,stdin=PIPE,stderr=PIPE,shell=True)
pipeData.stdin.write(args.select+'\n\q\n')

for line in pipeData.stdout:
	print line
exit(0)
#Shell=True vol dir que la comanda s'exexcuatra a traves de shell.
