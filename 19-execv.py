# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			23/01/2019		
#DESCRIPCIO: Exemople execv
###############################################################################################################################################
#Imports
import sys, os, signal, argparse
###############################################################################################################################################
#Printem el pid pare
print "Hola comensament del programa principal"
print "PID pare:", os.getpid()

#Creem el proces fill
pid = os.fork()

#Part pare
if pid != 0:
	print "Programa Pare", os.getpid(), pid
	sys.exit(0)

#Part fill
print "Programa Fill", os.getpid(), pid
#os.execv("/usr/bin/ls",["usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/") #Exemple execl
#os.execlp("ls","ls","-la","/") #Exemple execlp
os.execve("/usr/bin/ls",["/usr/bin/ls","-la","/"],{"nom":"/tmp","edat":"15"})

print "Hasta luego maricarmen Tururu"
sys.exit(0)
