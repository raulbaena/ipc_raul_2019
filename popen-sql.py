# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			16/01/2019		
#DESCRIPCIO: 10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups.
#LListar els grups del sistema ordenats pel criteri de gname, gid o de número d'usuaris.
#Atenció cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris
#que hi pertanyin, sense duplicitats, tant com a grup principal com a
#grup secundari.
###############################################################################################################################################
#Imports
import argparse
import sys
from subprocess import Popen, PIPE
###############################################################################################################################################
parser = argparse.ArgumentParser(description=\
        """Exemple popen""")
parser.add_argument("ruta",type=str,\
        help="directori a llistar")
args=parser.parse_args()
###############################################################################################################################################
sudo -u postgres psql -d centre_salut -U postgres -c "Select * FROM pacients"