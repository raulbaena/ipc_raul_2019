# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			21/01/2019		
#DESCRIPCIO: 16-exemlpe.1py 
###############################################################################################################################################
#Imports
import sys, os, signal, argparse
###############################################################################################################################################
parser = argparse.ArgumentParser(description="""Quantitat de segons d'un proces""")
parser.add_argument("segons",type=int,help="Numero de segons",metavar="segons")
args=parser.parse_args()
###############################################################################################################################################
#Definim les funcions
cont_up = 60
cont_down = 60 
def myusr1(signum,frame):
	print "Hem regut alarma", signum
	print "Sumem 60 sec al contador"
	segons_1 = args.segons + cont_up
	print segons_1
	
def myusr2(signum,frame):
	print "Hem regut alarma", signum
	print "Restem 60 sec al contador"
	segons_2 = args.segons - cont_down
	print segons_2
#Correció
#def myusr1(signum,frame):
#	print "Hem regut alarma", signum
#	print "Sumem 60 sec al contador"
#	actual = signal.alarm(0)
#	signal.alarm(actual+60)

#Correcció	
#def myusr2(signum,frame):
#	print "Hem regut alarma", signum
#	print "Sumem 60 sec al contador"
#	actual = signal.alarm(0)
#	signal.alarm(actual-60)

def myhup(signum,frame):
	print "Hem regut alarma", signum
	print "Reinitzalitzem el contador"
	segons_3 = args.segons
	print segons_3	

def myterm(signum,frame):
	print "Hem regut alarma", signum
	print "Faltan"
	segons = signal.alarm(args.segons)
	print segons
	
def myalarm(signum,frame):
	print "Hem regut alarma", signum
	print "Pleguem", cont_up, cont_down
###############################################################################################################################################	
#Xixa
#signal.signal(signal.SIGALARM,myalarm)
signal.signal(signal.SIGUSR2,myusr2)
signal.signal(signal.SIGUSR1,myusr1)
signal.signal(signal.SIGHUP,myhup)
signal.signal(signal.SIGTERM,myterm)
signal.alarm(args.segons)
signal.signal(signal.SIGINT,signal.SIG_IGN)
print os.getpid()

while True:
	pass
signal.alarm(0)

sys.exit(0)

