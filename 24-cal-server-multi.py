# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			25/01/2019		
#DESCRIPCIO: 23-date-server-multi
###############################################################################################################################################
#Imports
import sys,socket, os, signal, argparse
from subprocess import Popen, PIPE
###############################################################################################################################################

def myusr1(signum,frame):
	print "Hem regut alarma", signum
	print "Te voy a hacer un fork morena"
	
def myusr2(signum,frame):
	print "Hem regut alarma", signum
	print "Hasta luego maricarmen Tururu"
	sys.exit(0)
###############################################################################################################################################
pid = os.fork()

signal.signal(signal.SIGUSR2,myusr2)
signal.signal(signal.SIGUSR1,myusr1)

#Printem el pid fill
print "Programa Fill", os.getpid(), pid
while True:
	pass
###############################################################################################################################################
	HOST = ''
	PORT = 50001
	#Creem el socket
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	#Encadenem 
	s.bind((HOST, PORT))
	#Escoltem
	s.listen(1)
	while True:
		#Acceptem la conexio amb el client
		conn, addr = s.accept()
		print "CONNCTED BY", addr
		#Executem la comanda del date
		command = ['date']
		pipeData = Popen(command,stdout=PIPE)
		#Enviem la sortida de la comanda al client
		for line in pipeData.stdout:
			conn.send(line)
		conn.close()
sys.exit(0)
