# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			178/01/2019		
#DESCRIPCIO: 15-exemlpe.1py 
###############################################################################################################################################
#Imports
import sys, os, signal
def myhandler(signum,frame):
	print "Signal handler called with dignal:", signum
	print "hasta luego lucas!"
	sys.exit(1)
def mydeath(signum,frame):
	print "Signal handler called with dignal:", signum
	print "Tuh muertoh"

signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGUSR2,myhandler)
signal.signal(signal.SIGUSR1,mydeath)
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(180)
print os.getpid()
while True:
	pass
signal.alarm(0)

sys.exit(0)
