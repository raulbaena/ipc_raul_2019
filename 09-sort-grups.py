# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			14/01/2019		
#DESCRIPCIO: -09-sort-groups.py [-s login|gid|gname] [-f fileusers] [-g filegroup]
###############################################################################################################################################
#Imports
import argparse
import sys
###############################################################################################################################################
#Control d'arguments
parser = argparse.ArgumentParser(description= """Mostrar les N primereslínies """, epilog="thats all folks")
parser.add_argument("-u",type=str, help="fitxer d'ususaris a processar", metavar="fileusers", default="/dev/stdin",dest="fileusers")
parser.add_argument("-g",type=str, help="fitxer de grups a processar", metavar="filegroups", default="/dev/stdin",dest="filegroups")
parser.add_argument("-s",type=str, help="-s login|gid|gname", metavar="criteri", choices=["login,gd,gname"])
args = parser.parse_args()
###############################################################################################################################################
#Classes
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,linea):
    "Constructor objectes UnixUser"
    llista_user = linea.split(":")
    self.login= llista_user[0]
    self.passw= llista_user[1]
    self.uid= llista_user[2]
    self.gid= llista_user[3]
    self.gecos = llista_user[4]
    self.home = llista_user[5]
    self.shell = llista_user[6]
  def show(self):
    "Mostra dades del usuari"
    print "login: %s, %s, %d, %d, %s, %s, %s" % (self.login, self.passw, int(self.uid), int(self.gid), self.gecos, self.home, self.shell)
  def sumaun(self):
    "Funcio que suma 1 al uid"
    self.uid+=1
  def __str__(self):
    return "%s, %s, %d, %d, %s, %s, %s" % (self.login, self.passw, int(self.uid), int(self.gid), self.gecos, self.home, self.shell)
class UnixGroup():
	'''Clse unix group Dtipus mysql:x:27:gid:usuaris'''
	"Constructor objectes UnixGroup"
	def __init__(self,linea):
		llista_user = llista_group(":")
		self.gname = llista_group[0]
		self.passwd = llista_group[]
		self.gid = llista_group[2]
		self.usuaris = llista_group[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
	def show(self):
    	"Mostra dades del usuari"
   		print "Grup: %s, %s, %d, %s" % (self.gname, self.passwd, int(self.gid), self.usuaris)
	def __str__(self):
   	 	return " %s, %s, %d, %s" % (self.gname, self.passwd, int(self.gid), self.usuaris)
###############################################################################################################################################
#Funcions      
def cmp_login(a,b):
  '''Comparador d'usuaris segons el login'''
  if a.login > b.login:
    return 1
  if a.login < b.login:
    return -1
  return 0
def cmp_gid(a,b):
  '''Comparador d'usuaris segons el gid'''
  if a.gid > b.gid:
    return 1
  if a.gid < b.gid:
    return -1
  if a.login > b.login:
    return 1
  if a.login < b.login:
    return -1
  return 0
def cmp_gname(a,b):
  '''Comparador d'usuaris segons el gname'''
  if a.gname > b.gname:
    return 1
  if a.gname < b.gname:
    return -1
  if a.login > b.login:
    return 1
  if a.login < b.login:
    return -1
  return 0
###############################################################################################################################################
#Xixa
for line in groupFile:
  group=UnixGroup(line)
  groupDict[group.gid]=group
groupFile.close()
userFile=open(args.userFile,"r")
userList=[]
for line in userFile:
  user=UnixUser(line)
  userList.append(user)
userFile.close()
if args.criteria=="login":
  userList.sort(cmp_login)
elif args.criteria=="gid":
  userList.sort(cmp_gid)
else:
  userList.sort(cmp_gname)
for user in userList:
 print user
exit(0)
