# !/usr/bin/python			
# -*-coding: utf-8-*-
########################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			09/01/2019		
#DESCRIPCIO: Mostra les deu primeres linies de files o stdin
#Synopsis: head [file]
########################################################################
import sys
#CONSTANTS
MAXNUM = 10

#XIXA
filein = sys.stdin
cont_line = 0
if len(sys.argv) == 2:
	filein = open (sys.argv[1], "r")
for line in filein:
	print line
	cont_line += 1
	if cont_line == MAXNUM : break
filein.close()
exit(0)