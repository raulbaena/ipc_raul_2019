# /usr/bin/python
#-*- coding: utf-8-*-
#
# Exemple de creació de classes i objectes
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,linea):
    "Constructor objectes UnixUser"
    llista_user = linea.split(":")
    self.login= llista_user[0]
    self.passw= llista_user[1]
    self.uid= llista_user[2]
    self.gid= llista_user[3]
    self.gecos = llista_user[4]
    self.home = llista_user[5]
    self.shell = llista_user[6]
  def show(self):
    "Mostra dades del usuari"
    print "login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid)
  def sumaun(self):
    "Funcio que suma 1 al uid"
    self.uid+=1
  def __str__(self):
      return "%s %d %d" (self.login, self.uid, self.gid)


#user1=UnixUser("pere",15,100)
#user1.show()
#user1.sumaun()
#user1.show()
#user1
#l=[13,"juny",user1,user2
#l[2].show()

