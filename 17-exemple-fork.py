# !/usr/bin/python			
# -*-coding: utf-8-*-
###############################################################################################################################################
#NOM AUTOR:		Raul Baena Nocea									   
#ISX: 			isx53320159	
#DATA: 			23/01/2019		
#DESCRIPCIO: Exemople fork
###############################################################################################################################################
#Imports
import sys, os, signal, argparse
###############################################################################################################################################
print "Hola comensament del programa principal"
print "PID pare:", os.getpid()
#Crea una copia exactament igual que el programa que estem executant
pid = os.fork()
if pid != 0:
	#os.wait()
	print "Programa Pare", os.getpid(), pid
else:
	print "Programa Fill", os.getpid(), pid
print "Hasta luego maricarmen tururu"
sys.exit(0)

