#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2018-2019
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select

HOST = ''                 
PORT = 50001             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
		#Controla nova conxeio
        if actual == s:
            conn, addr = s.accept()
            print 'Connected by', addr
            conns.append(conn)
        #COntrola una conexio existent
        else:
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
				command = [data]
				pipeData = Popen(command,shell=True,stdout=PIPE)
				for line in pipeData.stdout:
					conn.send(line)
				conn.send(chr(4))
s.close()
sys.exit(0)
